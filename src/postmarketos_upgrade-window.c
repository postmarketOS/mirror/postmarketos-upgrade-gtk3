/* postmarketos_upgrade-window.c
 *
 * Copyright 2020 Martijn Braam
 * Copyright 2022 Luca Weiss
 * Copyright 2022 knuxify
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <glib/gstdio.h>
#include "postmarketos_upgrade-config.h"
#include "postmarketos_upgrade-window.h"

#define UPGRADE_MESSAGE_LOCATION "/etc/upgrade-message"

extern const gchar *ignore_file_path;

struct _PostmarketosUpgradeWindow
{
  AdwApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkCheckButton      *dont_show_again;
};

G_DEFINE_TYPE (PostmarketosUpgradeWindow, postmarketos_upgrade_window, ADW_TYPE_APPLICATION_WINDOW)

static void
on_upgrade_button (__attribute__ ((unused)) GtkButton *self)
{
  g_app_info_launch_default_for_uri("https://wiki.postmarketos.org/wiki/Upgrade_to_a_newer_postmarketOS_release", NULL, NULL);
}

static void
on_close_button (GtkButton *self)
{
  gtk_window_close (GTK_WINDOW (gtk_widget_get_native(GTK_WIDGET (self))));
}

static void
on_dont_show_again_toggled (GtkCheckButton* self, G_GNUC_UNUSED gpointer user_data)
{
  int ret;

  /* If the checkbox gets enabled create the file, otherwise remove the file. */
  if (gtk_check_button_get_active (self)) {
    ret = g_mkdir_with_parents (g_path_get_dirname (ignore_file_path), 0755);
    if (ret < 0)
      fprintf(stderr, "failed to create directory for %s: %s\n", ignore_file_path, strerror(errno));

    ret = g_open (ignore_file_path, O_CREAT | O_EXCL, 0666);
    if (ret < 0)
      fprintf(stderr, "failed to create %s: %s\n", ignore_file_path, strerror(errno));
  } else {
    ret = g_unlink (ignore_file_path);
    if (ret < 0)
      fprintf(stderr, "failed to unlink %s: %s\n", ignore_file_path, strerror(errno));
  }
}

static void
postmarketos_upgrade_window_class_init (PostmarketosUpgradeWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/postmarketos/Upgrade/postmarketos_upgrade-window.ui");
  gtk_widget_class_bind_template_child (widget_class, PostmarketosUpgradeWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, PostmarketosUpgradeWindow, dont_show_again);

  gtk_widget_class_bind_template_callback (widget_class, on_upgrade_button);
  gtk_widget_class_bind_template_callback (widget_class, on_close_button);
  gtk_widget_class_bind_template_callback (widget_class, on_dont_show_again_toggled);
}

static void
postmarketos_upgrade_window_init (PostmarketosUpgradeWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  /* Set up "Don't show again" checkbox */
  if (g_access(ignore_file_path, F_OK) == 0) {
    /* Block signal so we don't trigger callback */
    g_signal_handlers_block_matched (self->dont_show_again, G_SIGNAL_MATCH_FUNC, 0, 0, NULL,
        (gpointer) on_dont_show_again_toggled, NULL);

    gtk_check_button_set_active (self->dont_show_again, TRUE);

    /* Unblock signal again */
    g_signal_handlers_unblock_matched (self->dont_show_again, G_SIGNAL_MATCH_FUNC, 0, 0, NULL,
        (gpointer) on_dont_show_again_toggled, NULL);
  }
}
