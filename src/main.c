/* main.c
 *
 * Copyright 2020 Martijn Braam
 * Copyright 2022 Luca Weiss
 * Copyright 2022 knuxify
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <glib/gstdio.h>
#include <adwaita.h>
#include "postmarketos_upgrade-config.h"
#include "postmarketos_upgrade-window.h"

#define OS_RELEASE_LOCATION "/etc/os-release"
#define OS_RELEASE_VERSION "VERSION"

const gchar *ignore_file_path = NULL;

static const gchar *
get_os_release_field (const gchar *key)
{
  char *token;
  char *search = "=";
  FILE *file = fopen (OS_RELEASE_LOCATION, "r");
  if (!file) {
    fprintf(stderr, "could not open %s: %s\n", OS_RELEASE_LOCATION, strerror(errno));
    return NULL;
  }
  char line[128];
  while (fgets(line, sizeof (line), file) != NULL) {
    // Remove trailing newline
    line[strcspn(line, "\n")] = '\0';
    // Token will point to the part before the =
    token = strtok(line, search);
    g_debug ("token-a: %s\n", token);
    // Skip all unused lines
    if (!strcmp(token, key) == 0)
      continue;
    // Token will point to the part after the =
    token = strtok(NULL, search);
    g_debug ("token-b: %s\n", token);
    // Remove possible quotes
    if (token[0] == '"') {
      token++;
      int len = strlen(token);
      // Remove trailing quote
      if (token[len-1] == '"') {
        token[len-1] = '\0';
      }
    }

    fclose (file);

    GString *value = g_string_new (token);
    return g_string_free (value, FALSE);
  }
  fclose (file);
  fprintf (stderr, "could not find key %s in os-release\n", key);
  return NULL;
}

static void
on_activate (AdwApplication *app)
{
  GtkWindow *window;

  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));

  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (&app->parent_instance);
  if (window == NULL)
    window = g_object_new (POSTMARKETOS_UPGRADE_TYPE_WINDOW,
                           "application", app,
                           "default-width", 480,
                           "default-height", 800,
                           NULL);

  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (window);
}

static gint
handle_local_options (G_GNUC_UNUSED GApplication *application,
                      GVariantDict *options,
                      G_GNUC_UNUSED gpointer user_data)
{
  guint32 count;

  /* Deal with the 'autostart' option */
  if (g_variant_dict_lookup (options, "autostart", "b", &count)) {
    /* If the 'ignore' file exists, just quietly exit */
    if (g_access(ignore_file_path, F_OK) == 0) {
      g_print ("ignore file exists, exiting.\n");
      return EXIT_SUCCESS;
    }
  }

  return -1;
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr(AdwApplication) app = NULL;
  int ret;

  /* Set up gettext translations */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /* Get OS version and initialize ignore file path variable */
  const gchar *os_version = get_os_release_field (OS_RELEASE_VERSION);
  if (!os_version) {
    fprintf(stderr, "failed to get os version, using default.\n");
    os_version = "unknown";
  }
  g_print ("version from os-release: %s\n", os_version);

  GString *ignore_file_str = g_string_new (NULL);
  g_string_printf (ignore_file_str, "%s/postmarketos-upgrade/%s-ignore",
      g_get_user_data_dir (), os_version);
  ignore_file_path = g_string_free (ignore_file_str, FALSE);


  /*
   * Create a new GtkApplication. The application manages our main loop,
   * application windows, integration with the window manager/compositor, and
   * desktop features such as file opening and single-instance applications.
   */
  app = adw_application_new ("org.postmarketos.Upgrade", G_APPLICATION_FLAGS_NONE);

  g_application_add_main_option (G_APPLICATION (app), "autostart", 0, 0, G_OPTION_ARG_NONE,
      "Indicate app is launched as autostart", NULL);

  /*
   * We connect to the activate signal to create a window when the application
   * has been lauched. Additionally, this signal notifies us when the user
   * tries to launch a "second instance" of the application. When they try
   * to do that, we'll just present any existing window.
   *
   * Because we can't pass a pointer to any function type, we have to cast
   * our "on_activate" function to a GCallback.
   */
  g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);
  g_signal_connect (app, "handle-local-options", G_CALLBACK (handle_local_options), NULL);

  /*
   * Run the application. This function will block until the applicaiton
   * exits. Upon return, we have our exit code to return to the shell. (This
   * is the code you see when you do `echo $?` after running a command in a
   * terminal.
   *
   * Since GtkApplication inherits from GApplication, we use the parent class
   * method "run". But we need to cast, which is what the "G_APPLICATION()"
   * macro does.
   */
  ret = g_application_run (G_APPLICATION (app), argc, argv);

  return ret;
}
